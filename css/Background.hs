{-# LANGUAGE OverloadedStrings #-}

module Background where

import Clay

backgroundHearts =
  ".background" ? do
    backgroundImage $ url "../images/heart.svg"
    backgroundSize cover
    display block
    backgroundSize cover
    width (pct 100)
    height (pct 100)
    position fixed
    left (px 0)
    right (px 0)
    "z-index" -: "-1"
