{-# LANGUAGE OverloadedStrings #-}

-- | A wrapper over Clay Sizes for min and max css functions
module MinMax where

import Clay hiding (map)
import Data.Text hiding (map)

minimum_ :: Size a -> Size b -> Size c
minimum_ a b = other $ Value (call "max" (sizeIntercalate [value a, value b]))

maximum_ :: Size a -> Size b -> Size c
maximum_ a b = other $ Value (call "min" (sizeIntercalate [value a, value b]))

clamp_ :: Size a -> Size b -> Size c -> Size d
clamp_ a b c = other $ Value (call "clamp" (sizeIntercalate [value a, value b, value c]))

sizeIntercalate :: [Value] -> Prefixed
sizeIntercalate a = Clay.intercalate "," $ map unValue a