{-# LANGUAGE OverloadedStrings #-}

import Background
import Clay
import qualified Clay.Stylesheet
import Colors
import Font
import MinMax

link' :: Css
link' =
  a
    ? color cBlue

contentWrapper :: Css
contentWrapper = do
  ".content-wrapper"
    ? do
      centered

      display flex
      flexDirection column

      minWidth (pct 75)
      width fitContent

      minHeight (pct 50)
      height fitContent
      maxHeight (pct 100)
  header
    ? do
      display flex
      flexDirection row
      justifyContent spaceAround
      padding (px 25) (px 50) (px 25) (px 50)
  main_ ? do
    ".title" ? do
      textAlign center
    sym padding $ px 50
    border (px 2) solid cOverlay0
    sym borderRadius (px 25)

    display block
    background cMantle
    textAlign start

default' :: Css
default' = do
  backgroundHearts
  body ? do
    background cBase
    color cText
    textFont
    sym margin $ px 0
    display flex
    flexDirection column
    minHeight (vh 100)
  contentWrapper
  ".seperate"
    ? do
      width $ pct 100
      height $ px 2
      alignSelf center
      backgroundColor cOverlay0
      sym2 margin (px 50) (px 0)

footer' :: Css
footer' = do
  footer
    ? do
      centered
      color cSubtle
  ".footer-wrap"
    ? do
      width (pct 100)
      bottom (px 0)
      background cMantle
  ".haskell-logo"
    ? do
      height (em 1)
      position relative
      -- cursed magic values ??
      top $ em 0.153
  ".heart"
    ? color cPink

logo :: Css
logo =
  do
    ".logo-pink"
      ? color cPink
    ".logo"
      ? do
        fontSize (px 35)
        lineHeight inherit
        color cLavender
        textDecoration none

social :: Css
social =
  do
    ".social-logo"
      ? do
        fontSize (px 32)
        height (em 1)
        width (em 1)
        sym2 padding (px 0) (px 15)
    ".social-logo" # hover ? do
      filters [invert (pct 100)]

centered :: Css
centered =
  do
    textAlign center
    boxSizing borderBox
    sym2 margin (px 0) auto

main :: IO ()
main = putCss $ do
  "::selection" ? do
    "background-color" -: "color-mix(in srgb,#f5bde6,transparent 70%)"
  default'
  footer'
  link'
  isoveka
  uzura
  logo
  social