{-# LANGUAGE OverloadedStrings #-}

module Font where

import Clay
import Clay.Stylesheet
import Colors

isoveka :: Css
isoveka =
  fontFace $ do
    fontFaceSrc [FontFaceSrcUrl "../fonts/Iosevka.woff2" Nothing]
    fontFamily ["Iosevka"] [monospace]

uzura :: Css
uzura =
  fontFace $ do
    fontFaceSrc [FontFaceSrcUrl "../fonts/uzura.woff2" Nothing]
    fontFamily ["Uzura"] [sansSerif, serif]

textFont :: Clay.Stylesheet.StyleM ()
textFont =
  do
    fontSize (px 20)
    lineHeight (px 30)
    fontFamily ["Uzura"] [sansSerif]
    textRendering optimizeLegibility
    color cText
