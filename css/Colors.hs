{-# LANGUAGE OverloadedStrings #-}

module Colors where

import Clay

-- | https://catppuccin.com/palette
cBase, cSurface0, cOverlay0, cOverlay2, cText, cSubtext, cMantle, cBlue, cLavender, cPink, cSubtle :: Color
cBase = "#1e1e2e"
cSurface0 = "#313244"
cOverlay0 = "#6c7086"
cOverlay2 = "#9399b2"
cText = "#cdd6f4"
cSubtext = "#a6adc8"
cMantle = "#181825"
cBlue = "#89b4fa"
cLavender = "#b4befe"
cPink = "#f5c2e7"
cSubtle = "#7f849c"