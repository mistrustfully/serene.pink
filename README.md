# serene.pink

This is the source for my personal website, located at https://serene.pink/

Built using for [Hakyll](https://jaspervdj.be/hakyll/) for SSG and [Clay](http://fvisser.nl/clay/) for CSS

## Building
Requires GHC and Stack (See [ghcup](https://www.haskell.org/ghcup/) for installing)

First, install [Hakyll](https://jaspervdj.be/hakyll/tutorials/01-installation.html) with either of the following commands:
 - `stack install hakyll`
 - `cabal install hakyll`

Next run the following stack commands to build the website itself:
```
stack build
stack exec site build
```

The resulting site will be located at `_site/`

## License

All source code licensed under the [MIT License](LICENSE).

Logos from [Simple Icons](https://simpleicons.org/)

Icons from [Material Design Icons](https://pictogrammers.com/library/mdi/)

Uses the font [Iosevka](https://github.com/be5invis/Iosevka) 