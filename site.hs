--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}

import Data.List (isSuffixOf)
import Data.Monoid (mappend)
import Debug.Trace (trace)
import Hakyll
import System.FilePath.Posix

--------------------------------------------------------------------------------

-- | Text outputed to `_site/.domains`, useful for codeberg custom domains
domain :: String
domain = "serene.pink"

-- | The README.md in the sites output, for the codeberg pages repo
readme :: String
readme = "This site is automatically generated. See [serene.pink](https://codeberg.org/mistrustfully/serene.pink) for the source!"

--------------------------------------------------------------------------------

main :: IO ()
main = hakyll $ do
  create [".domains"] $ do
    route idRoute
    compile $ makeItem domain

  create ["README.md"] $ do
    route idRoute
    compile $ makeItem readme

  match "images/*/**" $ do
    route idRoute
    compile copyFileCompiler

  match "images/**" $ do
    route idRoute
    compile copyFileCompiler

  match "fonts/**" $ do
    route idRoute
    compile copyFileCompiler

  match "css/Main.hs" $ do
    route $ setExtension "css"
    compile (unixFilter "stack" ["runghc", "--", "-icss", "css/Main.hs"] "" >>= makeItem . compressCss)

  match "pages/*" $ do
    route $ cleanRoute `composeRoutes` gsubRoute "pages/" (const "")
    compile $ do
      posts <- recentFirst =<< loadAll "posts/*"
      let indexCtx =
            listField "posts" postCtx (return posts)
              `mappend` defaultContext

      pandocCompiler
        >>= applyAsTemplate indexCtx
        >>= loadAndApplyTemplate "templates/default.html" indexCtx
        >>= relativizeUrls
        >>= cleanIndexUrls

  match "posts/*" $ do
    route cleanRoute
    compile $
      pandocCompiler
        >>= loadAndApplyTemplate "templates/post.html" postCtx
        >>= loadAndApplyTemplate "templates/default.html" postCtx
        >>= relativizeUrls
        >>= cleanIndexUrls

  match "index.html" $ do
    route idRoute
    compile $ do
      posts <- recentFirst =<< loadAll "posts/*"
      let indexCtx =
            listField "posts" postCtx (return posts)
              `mappend` defaultContext

      getResourceBody
        >>= applyAsTemplate indexCtx
        >>= loadAndApplyTemplate "templates/default.html" indexCtx
        >>= relativizeUrls
        >>= cleanIndexUrls

  match "templates/*" $ compile templateBodyCompiler

--------------------------------------------------------------------------------
postCtx :: Context String
postCtx =
  dateField "date" "%B %e, %Y"
    `mappend` defaultContext

pagesCtx :: Compiler (Context String)
pagesCtx = do
  posts <- recentFirst =<< loadAll "posts/*"
  return (listField "posts" postCtx (return posts) `mappend` constField "title" "Archives" `mappend` defaultContext)

--------------------------------------------------------------------------------

cleanRoute :: Routes
cleanRoute = customRoute createIndexRoute
  where
    createIndexRoute ident = takeDirectory p </> takeBaseName p </> "index.html"
      where
        p = toFilePath ident

cleanIndexUrls :: Item String -> Compiler (Item String)
cleanIndexUrls = return . fmap (withUrls cleanIndex)

cleanIndexHtmls :: Item String -> Compiler (Item String)
cleanIndexHtmls = return . fmap (replaceAll pattern replacement)
  where
    pattern = "/index.html"
    replacement = const "/"

cleanIndex :: String -> String
cleanIndex url
  | idx `isSuffixOf` url = take (length url - length idx) url
  | otherwise = url
  where
    idx = "index.html"