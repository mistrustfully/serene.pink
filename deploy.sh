#!/usr/bin/env bash

# Creates a new git repository and force pushes it to a codeberg pages repo

export GPG_TTY=$(tty)

if ! command -v stack &> /dev/null
then
    echo "Stack is not installed."
    exit 1
fi

build_dir="_site/"
url="git@codeberg.org:mistrustfully/pages.git"

echo "Building site"
stack run site rebuild

echo "Initialzing git repository"
git -C "${build_dir}" init
git -C "${build_dir}" checkout -b main

echo "Commiting site changes"
git -C "${build_dir}" add .
git -C "${build_dir}" commit -m "build: update static site." -S

echo "Force pushing it to main"
git -C "${build_dir}" remote add origin "${url}"
git -C "${build_dir}" push --force origin main